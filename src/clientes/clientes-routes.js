import express from 'express'
const router = express.Router()

const controller = require('./cliente-controller')


router.post('/', (req, res, next) => {
    controller.cadastrarCliente(req.body)
        .then(response => res.status(200).send(response))
        .catch(next)

})

router.get('/', (req, res, next) => {
    controller.listarTodos()
        .then(clientes => res.status(200).send(clientes))
        .catch(next)

})

router.get('/:id', (req, res, next) => {
    controller.buscarCliente(req.params.id)
        .then(cliente => res.status(200).send(cliente))
        .catch(next)

})

router.put('/:_id', (req, res, next) => {
    controller.atualizarCliente(req.params._id, req.body)
        .then(response => res.status(200).send(response))
        .catch(next)

})

router.delete('/:id', (req, res, next) => {
    controller.removerCliente(req.params.id)
        .then(response => res.status(200).send(response))
        .catch(next)

})

export default router