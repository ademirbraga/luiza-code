const clienteModel = require('./cliente-service')

const cadastrarCliente = (cliente) => clienteModel.criarCliente(cliente)

const listarTodos = () => clienteModel.listarTodos()

const buscarCliente = (id) => clienteModel.buscarCliente(id)

const atualizarCliente = (id, cliente) => clienteModel.atualizarCliente(id, cliente)

const removerCliente = (id) => clienteModel.removerCliente(id)

module.exports = {
    cadastrarCliente,
    listarTodos,
    buscarCliente,
    atualizarCliente,
    removerCliente
}