const mongoose = require('mongoose')
const config = require('../config')
mongoose.connect(config.connectionString)
const Cliente = require('./cliente-model')

const criarCliente = (cliente) => {
    const newCliente = new Cliente(cliente)
    return newCliente.save()
}
const listarTodos = () => Cliente.find()

const buscarCliente = (params) => Cliente.findOne(params)

const atualizarCliente = (id, update) => Cliente.findOneAndUpdate({ '_id': id }, update)

const removerCliente = (params) => Cliente.deleteOne(params)


module.exports = {
    criarCliente,
    listarTodos,
    buscarCliente,
    atualizarCliente,
    removerCliente
}