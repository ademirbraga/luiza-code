import express from 'express'
import RouterProduto from '../produtos/produtos-routes'
import RouterCliente from '../clientes/clientes-routes'

const router = express.Router()

router.get('/', (req, res) => res.send('Hello world!'))

router.get('/teste', (req, res) => res.send('Hello teste!'))

router.use('/produtos', RouterProduto)

router.use('/clientes', RouterCliente)

export default router